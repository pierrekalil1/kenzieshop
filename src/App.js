import { createGlobalStyle } from "styled-components";
import "./App.css";
import Products from "./components/Products";
import Header from "./components/Header";
import Routes from "./Routes";

function App() {
  return (
    <div className="App">
      <createGlobalStyle />
      <Header />
      {/* <h1>Title</h1> */}
      <Products />
      <Routes />
    </div>
  );
}

export default App;
