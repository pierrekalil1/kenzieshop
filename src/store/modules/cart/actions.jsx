export const addToCart = (product) => ({
  type: "@cart/ADD",
  product,
});

export const removeFromCart = (list) => {
  return {
    type: "@cart/REMOVE",
    list,
  };
};
