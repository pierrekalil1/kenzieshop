const initialState = [
  {
    id: 1,
    name: "Marvels",
    price: 149.0,
    image: "https://images-na.ssl-images-amazon.com/images/I/91KTBkD8amL.jpg",
  },
  {
    id: 2,
    name: "A Nova Fronteira",
    price: 97.0,
    image: "https://images-na.ssl-images-amazon.com/images/I/91gLq0Xh+PL.jpg",
  },
  {
    id: 3,
    name: "X-Men Grand Design",
    price: 150.0,
    image: "https://images-na.ssl-images-amazon.com/images/I/81-L6hMMXkL.jpg",
  },
  {
    id: 4,
    name: "Batman - Ano Um",
    price: 99.0,
    image: "https://images-na.ssl-images-amazon.com/images/I/61-2G84LF-L.jpg",
  },
  {
    id: 5,
    name: "Homem-Aranha: História de Vida",
    price: 109.0,
    image: "https://m.media-amazon.com/images/I/51v1isGv3yS._SL500_.jpg",
  },
  {
    id: 6,
    name: "Marvel 1602",
    price: 99.9,
    image: "https://images-na.ssl-images-amazon.com/images/I/91c7gWy+CPL.jpg",
  },
  {
    id: 7,
    name: "Watchmen: The Deluxe Edition",
    price: 190.9,
    image: "https://m.media-amazon.com/images/I/61Oe1htk6ML.jpg",
  },
  {
    id: 8,
    name: "Batman Noir. O Cavaleiro das Trevas",
    price: 70.9,
    image: "https://img.assinaja.com/assets/tZ/004/img/157831_520x520.jpg",
  },
];

const productsReducer = (state = initialState, action) => {
  return state;
};

export default productsReducer;
