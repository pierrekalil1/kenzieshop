import { Switch, Route } from "react-router-dom";

import Cart from "../components/Cart";
import PrimarySearchAppBar from "../components/Header";

const Routes = () => {
  return (
    <Switch>
      <Route path="/header">
        <PrimarySearchAppBar />
      </Route>
      <Route path="/cart">
        <Cart />
      </Route>
    </Switch>
  );
};

export default Routes;
