import { Button } from "@material-ui/core";
import { useDispatch } from "react-redux";
import { addToCartThunk } from "../../store/modules/cart/thunks";
import { Container, CardInfo } from "./style";

const Product = ({ product }) => {
  const dispatch = useDispatch();
  const { name, price, image } = product;

  return (
    <Container>
      <img src={image} alt={name} />
      <CardInfo>
        <span>{name}</span>
        <span>R$ {price}</span>

        <Button
          type="submit"
          variant="contained"
          color="secondary"
          onClick={() => dispatch(addToCartThunk(product))}
        >
          Adicionar ao carrinho
        </Button>
      </CardInfo>
    </Container>
  );
};

export default Product;
