import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  flex-wrap: wrap;
  width: 230px;
  height: 360px;
  margin: 100px 10px 0 10px;
  padding: 5px;
  background-color: #fff;
  box-shadow: 2px 2px 5px #41413e;

  img {
    width: 100%;
    height: 70%;
  }

  span:nth-child(1) {
    font-weight: bold;
    /* color: #fff; */
    margin-bottom: 5px;
  }
  span {
    /* color: #fff; */
  }
`;

export const CardInfo = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
`;
