import { Container } from "./style";
import { useSelector } from "react-redux";
import Product from "../Product";

const Products = () => {
  const products = useSelector((store) => store.products);

  return (
    <Container>
      {products.map((item) => {
        return <Product key={item.id} product={item} />;
      })}
    </Container>
  );
};

export default Products;
